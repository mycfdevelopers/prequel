package com.mycf.prequel;

import java.util.List;

public class Utils {

    /**
     * Private constructor for utility class
     */
    private Utils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Join the given list of Strings with the given conjunction (similar to
     * Python's join() method)
     * @param list List of Strings to join
     * @param conjunction String to join the List with
     * @return Conjoined String
     */
    public static String join(List<String> list, String conjunction) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (String item : list) {
            if (first)
                first = false;
            else
                sb.append(conjunction);
            sb.append(item);
        }
        return sb.toString();
    }
}
