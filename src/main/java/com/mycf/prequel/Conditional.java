package com.mycf.prequel;

import com.mycf.prequel.exceptions.NoSuchOperatorException;
import com.mycf.prequel.exceptions.UnhandledValueTypeException;

import java.util.logging.Logger;

public class Conditional {

    private static Logger logger = Logger.getLogger(Conditional.class.getName());

    public enum Operator {
        EQUALS, NOT_EQUALS;
    }

    Column column;
    Object valueToCheck;
    Operator operator;

    /**
     * Constructor for Conditional
     * @param column Column to check
     * @param operator Operator to use
     * @param valueToCheck Value to check against
     */
    public Conditional(Column column, Operator operator, Object valueToCheck) {
        this.column = column;
        this.operator = operator;
        this.valueToCheck = valueToCheck;
    }

    /**
     * Constructor for Conditional without 'valueToCheck'
     * @param column Column to check
     * @param operator Operator to use
     */
    public Conditional(Column column, Operator operator) {
        this.column = column;
        this.operator = operator;
        this.valueToCheck = null;
    }

    /**
     * Gets the Column used for the conditional
     * @return Column column
     */
    public Column getColumn() {
        return column;
    }

    /**
     * Gets the Operator used for the conditional
     * @return Operator operator
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * Gets the value the conditional checks the column against
     * @return Object valueToCheck
     */
    public Object getValueToCheck() {
        return valueToCheck;
    }

    /**
     * Sets the Column to use in the conditional
     * @param column Column to use
     */
    public void setColumn(Column column) {
        this.column = column;
    }

    /**
     * Sets the Operator to use in the conditional
     * @param operator Operator to use
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * Sets the value to check against in the conditional
     * @param valueToCheck Value to check against
     */
    public void setValueToCheck(Object valueToCheck) {
        this.valueToCheck = valueToCheck;
    }

    /**
     * Get the String representation of an Operator
     * @param op Operator to get the String representation of
     * @return String representation of operator
     */
    public static String getOperatorStr(Operator op) throws NoSuchOperatorException {
        switch(op) {
            case EQUALS:
                return "=";
            case NOT_EQUALS:
                return "!=";
        }
        throw new NoSuchOperatorException("No such operator!");
    }

    /**
     * Get the String representation of this Conditional for use in a Query
     * @return String conditional for query
     */
    public String getForQuery() {
        StringBuilder str = new StringBuilder();
        try {
            str.append(column.getNameForQuery())
               .append(getOperatorStr(operator));

            if (valueToCheck == null) {
                str.append("?");
            }
            else if (valueToCheck instanceof String) {
                str.append("'").append(valueToCheck).append("'");
            } else {
                throw new UnhandledValueTypeException("Unhandled type: " + valueToCheck.getClass().toString());
            }
        } catch (NoSuchOperatorException | UnhandledValueTypeException e) {
            logger.severe(e.getMessage());
        }

        return str.toString();
    }
}
