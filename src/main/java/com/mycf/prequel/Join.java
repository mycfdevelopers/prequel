package com.mycf.prequel;

public abstract class Join {

    private String tableToJoinWith;
    private Column referenceColumn;
    private String localTable;
    private Column localColumn;

    public Join(String tableToJoinWith, Column referenceColumn, String localTable, Column localColumn) {
        this.tableToJoinWith = tableToJoinWith;
        this.referenceColumn = referenceColumn;
        this.localTable = localTable;
        this.localColumn = localColumn;
    }

    public String getTableToJoinWith() {
        return tableToJoinWith;
    }

    public Column getReferenceColumn() {
        return referenceColumn;
    }

    public String getLocalTable() {
        return localTable;
    }

    public Column getLocalColumn() {
        return localColumn;
    }

    public void setTableToJoinWith(String tableToJoinWith) {
        this.tableToJoinWith = tableToJoinWith;
    }

    public void setReferenceColumn(Column referenceColumn) {
        this.referenceColumn = referenceColumn;
    }

    public void setLocalTable(String localTable) {
        this.localTable = localTable;
    }

    public void setLocalColumn(Column localColumn) {
        this.localColumn = localColumn;
    }

    public abstract String render();

}
