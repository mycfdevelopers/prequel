package com.mycf.prequel;

import java.util.ArrayList;
import java.util.List;

public class InsertQuery extends Query {

    /**
     * Public constructor for InsertQuery
     *
     * @param tableName Name of the table to query
     * @param columns   Columns to be a part of the Query
     */
    public InsertQuery(String tableName, List<Column> columns) {
        super(tableName, columns);
    }

    public String render() {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("INSERT INTO \"")
                    .append(tableName)
                    .append("\"(");

        List<String> colNamesForQuery = new ArrayList<>();
        for (Column col : columns) {
            colNamesForQuery.add(col.getNameForQuery());
        }

        queryBuilder.append(Utils.join(colNamesForQuery, ", "));

        queryBuilder.append(")");

        // Values
        queryBuilder.append(" VALUES (");

        for (int i = 0; i < columns.size(); i++) {
            if (i > 0) {
                queryBuilder.append(", ");
            }
            queryBuilder.append("?");
        }

        queryBuilder.append(");");

        return queryBuilder.toString();
    }
}
