package com.mycf.prequel.exceptions;

public class NoSuchOperatorException extends Exception {

    /**
     * Raised when trying to use an Operator that does not exist
     * @param message Error message
     */
    public NoSuchOperatorException(String message) {
        super(message);
    }

}
