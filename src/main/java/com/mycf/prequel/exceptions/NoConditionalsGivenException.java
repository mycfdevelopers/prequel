package com.mycf.prequel.exceptions;

public class NoConditionalsGivenException extends Exception {

    /**
     * Raised when trying to render a Query without Conditionals that requires at least one Conditional
     * @param message Error message
     */
    public NoConditionalsGivenException(String message) {
        super(message);
    }

}
