package com.mycf.prequel.exceptions;

public class UnhandledValueTypeException extends Exception {
    /**
     * Raised when trying to process a type of value that is not handled
     * @param message Error message
     */
    public UnhandledValueTypeException(String message) {
        super(message);
    }
}
