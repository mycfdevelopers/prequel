package com.mycf.prequel.exceptions;

public class NoColumnsGivenException extends Exception {

    /**
     * Raised when trying to render a Query without Columns that requires at least one Column
     * @param message Error message
     */
    public NoColumnsGivenException(String message) {
        super(message);
    }

}
