package com.mycf.prequel;

public class Column {

    private String name;
    private Type type;
    private boolean isPk;
    private boolean canBeNull;

    /**
     * Public Constructor for a Column
     * @param name Name of the column
     * @param type Type of the column
     * @param isPk Whether or not the column serves as a primary key
     * @param canBeNull Whether or not the column can contain null values
     */
    public Column(String name, Type type, boolean isPk, boolean canBeNull) {
        this.name = name;
        this.type = type;
        this.isPk = isPk;
        this.canBeNull = canBeNull;
    }

    /**
     * Gets the name of the Column
     * @return String name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the Type of the Column
     * @return Type type
     */
    public Type getType() {
        return type;
    }

    /**
     * Gets whether the Column is used as a primary key
     * @return boolean isPrimaryKey
     */
    public boolean isPrimaryKey() {
        return isPk;
    }

    /**
     * Gets whether the Column can contain null values
     * @return boolean conBeNull
     */
    public boolean canBeNull() {
        return canBeNull;
    }

    /**
     * Sets the name of the Column
     * @param name String name of Column
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the Type of the Column
     * @param type Type of the Column
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * Sets this Column as a primary key
     */
    public void setAsPrimaryKey() {
        this.isPk = true;
    }

    /**
     * Removes this Column as a primary key (if it was one)
     */
    public void setAsNotPrimaryKey() {
        this.isPk = false;
    }

    /**
     * Allows for this Column to contain null values
     */
    public void allowNullValues() {
        this.canBeNull = true;
    }

    /**
     * Requires this Column to contain exclusively non-null values
     */
    public void disallowNullValues() {
        this.canBeNull = false;
    }

    /**
     * Get the name of the Column for use in a Query (surrounded by '\"')
     * @return String Column Name for Query
     */
    public String getNameForQuery() {
        return "\"" + name + "\"";
    }
}
