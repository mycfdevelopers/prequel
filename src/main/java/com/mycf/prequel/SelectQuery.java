package com.mycf.prequel;

import java.util.ArrayList;
import java.util.List;

public class SelectQuery extends Query {

    /**
     * Public constructor for SelectQuery
     *
     * @param tableName Name of the table to query
     * @param columns   Columns to be a part of the Query
     */
    public SelectQuery(String tableName, List<Column> columns) {
        super(tableName, columns);
    }

    /**
     * Constructs a SelectQuery without defined Columns. This is useful when
     * you want to run 'SELECT * FROM table'.
     * @param tableName Name of the table to query
     */
    public SelectQuery(String tableName) {
        super(tableName, new ArrayList<Column>());
    }

    /**
     * Render the Select Query
     * @return String query
     */
    public String render() {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");

        if (columns.isEmpty()) {
            // No columns specified, assume we want all
            query.append("*");
        } else {
            ArrayList<String> colNames = new ArrayList<>();
            for (Column col : columns) {
                colNames.add(col.getNameForQuery());
            }

            query.append(Utils.join(colNames, ", "));
        }

        query.append(" FROM ").append("\"").append(tableName).append("\"");

        // Add Joins
        if (!joins.isEmpty()) {
            for (Join join : joins) {
                query.append(" ").append(join.render());
            }
        }

        // Add conditionals
        if (!conditionals.isEmpty()) {
            List<String> conditionalStrings = new ArrayList<>();
            for (Conditional conditional : conditionals) {
                conditionalStrings.add(conditional.getForQuery());
            }

            query.append(" WHERE ").append(Utils.join(conditionalStrings, " AND "));
        }

        query.append(";");

        return query.toString();
    }
}
