package com.mycf.prequel;

import com.mycf.prequel.exceptions.NoColumnsGivenException;
import com.mycf.prequel.exceptions.NoConditionalsGivenException;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class UpdateQuery extends Query {

    private static Logger logger = Logger.getLogger(Query.class.getName());

    /**
     * Public constructor for UpdateQuery
     *
     * @param tableName Name of the table to query
     * @param columns   Columns to be a part of the Query
     */
    public UpdateQuery(String tableName, List<Column> columns) {
        super(tableName, columns);
    }

    /**
     * Constructor for UpdateQuery with all required fields
     *
     * @param tableName    Name of the table to Query
     * @param columns      Columns to be a part of the Query
     * @param conditionals Conditionals to be applied to the Query
     */
    public UpdateQuery(String tableName, List<Column> columns, List<Conditional> conditionals) {
        super(tableName, columns);
        this.conditionals = conditionals;
    }

    /**
     * Render the Update Query
     *
     * @return String query
     */
    public String render() throws NoColumnsGivenException, NoConditionalsGivenException {
        StringBuilder query = new StringBuilder();
        query.append("UPDATE ").append("\"").append(tableName).append("\"").append(" SET ");

        if (columns.isEmpty()) {
            // Must specify columns for an update
            throw new NoColumnsGivenException("Must specify at least one Column for an Update query!");
        } else if (conditionals.isEmpty()) {
            throw new NoConditionalsGivenException("Must give at least one Conditional for an Update query!");
        } else {
            ArrayList<String> colNames = new ArrayList<>();
            for (Column col : columns) {
                colNames.add(col.getNameForQuery() + "=?");
            }

            query.append(Utils.join(colNames, ", "));
        }
        // Add conditionals
        List<String> conditionalStrings = new ArrayList<>();
        for (Conditional conditional : conditionals) {
            conditionalStrings.add(conditional.getForQuery());
        }

        query.append(" WHERE ").append(Utils.join(conditionalStrings, " AND "));

        query.append(";");

        return query.toString();
    }
}
