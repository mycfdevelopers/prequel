package com.mycf.prequel;

public class InnerJoin extends Join {

    public InnerJoin(String tableToJoinWith, Column referenceColumn, String localTable, Column localColumn) {
        super(tableToJoinWith, referenceColumn, localTable, localColumn);
    }

    @Override
    public String render() {
        StringBuilder join = new StringBuilder();

        join.append("INNER JOIN \"").append(getTableToJoinWith()).append("\" ");
        join.append("ON \"").append(getTableToJoinWith()).append("\".\"").append(getReferenceColumn().getName());
        join.append("\"=\"").append(getLocalTable()).append("\".\"").append(getLocalColumn().getName()).append("\"");

        return join.toString();
    }
}
