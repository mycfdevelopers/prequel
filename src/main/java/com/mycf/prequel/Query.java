/*
 * Media You Can Feel, LLC
 *
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */
package com.mycf.prequel;

import com.mycf.prequel.exceptions.NoColumnsGivenException;
import com.mycf.prequel.exceptions.NoConditionalsGivenException;

import java.util.ArrayList;
import java.util.List;

public abstract class Query {
    String tableName;
    List<Column> columns;
    List<Conditional> conditionals;
    List<Join> joins;

    /**
     * Public constructor for Query
     * @param tableName Name of the table to query
     * @param columns Columns to be a part of the Query
     */
    public Query(String tableName, List<Column> columns) {
        this.tableName = tableName;
        this.columns = columns;
        this.conditionals = new ArrayList<>();
        this.joins = new ArrayList<>();
    }

    /**
     * Gets the Table Name used for this Query
     * @return String tableName
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * Gets the Columns used for this Query
     * @return List of Columns
     */
    public List<Column> getColumns() {
        return columns;
    }

    /**
     * Gets all Conditionals associated with this Query
     * @return List of Conditionals
     */
    public List<Conditional> getConditionals() {
        return conditionals;
    }

    /**
     * Gets all Joins associated with this Query
     * @return List of Joins
     */
    public List<Join> getJoins() {
        return joins;
    }

    /**
     * Sets the Table Name used by the Query
     * @param tableName Name of the Table to Query
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /**
     * Sets the Columns to be Queried
     * @param columns List of Columns to be queried
     */
    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    /**
     * Sets the Conditionals for this Query
     * @param conditionals List of Conditioanls
     */
    public void setConditionals(List<Conditional> conditionals) {
        this.conditionals = conditionals;
    }

    /**
     * Sets the Joins for this Query
     * @param joins List of Joins
     */
    public void setJoins(List<Join> joins) {
        this.joins = joins;
    }

    /**
     * Add a Conditional to this Query
     * @param conditional Conditional to add
     */
    public void addConditional(Conditional conditional) {
        this.conditionals.add(conditional);
    }

    /**
     * Adds a Join to this Query
     * @param join Join to add
     */
    public void addJoin(Join join) {
        this.joins.add(join);
    }

    /**
     * Renders the Query into a String
     */
    public abstract String render() throws NoColumnsGivenException, NoConditionalsGivenException;
}
