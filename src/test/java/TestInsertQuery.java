import com.mycf.prequel.Column;
import com.mycf.prequel.Conditional;
import com.mycf.prequel.InsertQuery;
import com.mycf.prequel.Type;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class TestInsertQuery {

    private static Type type1 = new Type();
    private static Column col1 = new Column("First Name", type1, false, false);
    private static Column col2 = new Column("Email", type1, true, false);

    private static ArrayList colList1 = new ArrayList();
    private static ArrayList colList2 = new ArrayList();

    @BeforeSuite
    static void setup() {
        colList1.add(col1);
        colList1.add(col2);

        colList2.add(col1);
    }

    /**
     * Tests constructor and base 'get' methods
     */
    @Test
    public void testConstructorAndGets() {
        String tableName = "Users";
        InsertQuery q1 = new InsertQuery(tableName, colList1);

        Assert.assertEquals(q1.getTableName(), tableName);
        Assert.assertEquals(q1.getColumns(), colList1);
        Assert.assertTrue(q1.getConditionals().isEmpty());
    }

    /**
     * Tests base 'set' methods
     */
    @Test
    public void testSets() {
        String tableName = "Users";
        InsertQuery q1 = new InsertQuery(tableName, colList1);

        // Table Name
        String newTableName = "User Information";
        q1.setTableName(newTableName);
        Assert.assertEquals(q1.getTableName(), newTableName);

        // Columns
        Assert.assertNotEquals(colList1, colList2);
        q1.setColumns(colList2);
        Assert.assertEquals(q1.getColumns(), colList2);

        // Conditionals
        Assert.assertTrue(q1.getConditionals().isEmpty());
        Conditional cond1 = new Conditional(col1, Conditional.Operator.EQUALS, "Jim");
        List<Conditional> conditionals = new ArrayList<>();
        conditionals.add(cond1);
        q1.setConditionals(conditionals);
        Assert.assertEquals(q1.getConditionals(), conditionals);
    }

    /**
     * Tests 'render' method
     */
    @Test
    public void testRender() {
        Column col1 = new Column("First Name", type1, false, false);
        Column col2 = new Column("ID", type1, true, false);
        List<Column> cols = new ArrayList<Column>();
        cols.add(col1);
        cols.add(col2);

        InsertQuery query = new InsertQuery("Users", cols);

        String output = query.render();
        String correctOutput = "INSERT INTO \"Users\"(\"First Name\", \"ID\") VALUES (?, ?);";

        Assert.assertEquals(output, correctOutput);

        // Single Column
        List<Column> cols2 = new ArrayList<Column>();
        cols2.add(col1);

        InsertQuery query2 = new InsertQuery("Users", cols2);
        
        String output2 = query2.render();
        String correctOutput2 = "INSERT INTO \"Users\"(\"First Name\") VALUES (?);";

        Assert.assertEquals(output2, correctOutput2);
    }
}
