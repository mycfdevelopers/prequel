import com.mycf.prequel.*;
import com.mycf.prequel.exceptions.NoColumnsGivenException;
import com.mycf.prequel.exceptions.NoConditionalsGivenException;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class TestUpdateQuery {

    private static Type type1 = new Type();
    private static Column col1 = new Column("First Name", type1, false, false);
    private static Column col2 = new Column("Email", type1, true, false);

    private static ArrayList<Column> colList1 = new ArrayList<>();
    private static ArrayList<Column> colList2 = new ArrayList<>();

    private static Conditional cond1 = new Conditional(col1, Conditional.Operator.EQUALS, "Jim");
    private static Conditional cond2 = new Conditional(col2, Conditional.Operator.NOT_EQUALS, "test@email.com");

    private static ArrayList<Conditional> condList1 = new ArrayList<>();

    @BeforeSuite
    static void setup() {
        colList1.add(col1);
        colList1.add(col2);

        colList2.add(col1);

        condList1.add(cond1);
        condList1.add(cond2);
    }

    /**
     * Tests constructor and base 'get' methods
     */
    @Test
    public void testConstructorAndGets() {
        String tableName = "Users";
        UpdateQuery q1 = new UpdateQuery(tableName, colList1);

        Assert.assertEquals(q1.getTableName(), tableName);
        Assert.assertEquals(q1.getColumns(), colList1);
        Assert.assertTrue(q1.getConditionals().isEmpty());

        // With Conditionals
        UpdateQuery q2 = new UpdateQuery(tableName, colList1, condList1);

        Assert.assertEquals(q2.getTableName(), tableName);
        Assert.assertEquals(q2.getColumns(), colList1);
        Assert.assertEquals(q2.getConditionals(), condList1);
    }

    /**
     * Tests base 'set' methods`
     */
    @Test
    public void testSets() {
        String tableName = "Users";
        UpdateQuery q1 = new UpdateQuery(tableName, colList1);

        // Table Name
        String newTableName = "User Information";
        q1.setTableName(newTableName);
        Assert.assertEquals(q1.getTableName(), newTableName);

        // Columns
        Assert.assertNotEquals(colList1, colList2);
        q1.setColumns(colList2);
        Assert.assertEquals(q1.getColumns(), colList2);

        // Conditionals
        Assert.assertTrue(q1.getConditionals().isEmpty());
        Conditional cond1 = new Conditional(col1, Conditional.Operator.EQUALS, "Jim");
        List<Conditional> conditionals = new ArrayList<>();
        conditionals.add(cond1);
        q1.setConditionals(conditionals);
        Assert.assertEquals(q1.getConditionals(), conditionals);
    }

    /**
     * Tests that a render fails when no Columns are given
     *
     * @throws NoColumnsGivenException Thrown when no Columns are given
     * @throws NoConditionalsGivenException Thrown when no Conditionals are given
     */
    @Test (expectedExceptions = NoColumnsGivenException.class)
    public void testRenderWithNoColumns() throws NoColumnsGivenException, NoConditionalsGivenException {
        String tableName = "Users";

        // No Columns
        UpdateQuery query = new UpdateQuery(tableName, new ArrayList<Column>());
        query.render();
    }

    /**
     * Tests that a render fails when no Conditionals are given
     *
     * @throws NoColumnsGivenException Thrown when no Columns are given
     * @throws NoConditionalsGivenException Thrown when no Conditionals are given
     */
    @Test (expectedExceptions = NoConditionalsGivenException.class)
    public void testRenderWithNoConditionals() throws NoColumnsGivenException, NoConditionalsGivenException {
        String tableName = "Users";

        // No Columns
        UpdateQuery query = new UpdateQuery(tableName, colList1);
        query.render();
    }

    /**
     * Test a successful render
     *
     * @throws NoColumnsGivenException Thrown when no Columns are given
     * @throws NoConditionalsGivenException Thrown when no Conditionals are given
     */
    @Test
    public void testRender() throws NoColumnsGivenException, NoConditionalsGivenException {
        String tableName = "Users";

        UpdateQuery query = new UpdateQuery(tableName, colList1, condList1);
        String output = query.render();
        String correctOutput = "UPDATE \"Users\" SET \"First Name\"=?, \"Email\"=? WHERE \"First Name\"='Jim' AND \"Email\"!='test@email.com';";

        Assert.assertEquals(output, correctOutput);
    }
}
