import com.mycf.prequel.*;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class TestSelectQuery {
    private static Type type1 = new Type();
    private static Column col1 = new Column("First Name", type1, false, false);
    private static Column col2 = new Column("Email", type1, true, false);

    private static ArrayList colList1 = new ArrayList();
    private static ArrayList colList2 = new ArrayList();

    private static Conditional cond1 = new Conditional(col1, Conditional.Operator.EQUALS, "Jim");
    private static Conditional cond2 = new Conditional(col2, Conditional.Operator.NOT_EQUALS, "test@email.com");

    private static InnerJoin join1 = new InnerJoin("Users", col1, "Emails", col2);

    @BeforeSuite
    static void setup() {
        colList1.add(col1);
        colList1.add(col2);

        colList2.add(col1);
    }

    /**
     * Tests basic constructor + get methods
     */
    @Test
    public void testConstructorAndGets() {
        String tableName = "Users";
        SelectQuery q1 = new SelectQuery(tableName, colList1);

        Assert.assertEquals(q1.getTableName(), tableName);
        Assert.assertEquals(q1.getColumns(), colList1);
        Assert.assertTrue(q1.getConditionals().isEmpty());
        Assert.assertTrue(q1.getJoins().isEmpty());

        SelectQuery q2 = new SelectQuery(tableName);
        Assert.assertEquals(q2.getTableName(), tableName);
        Assert.assertEquals(q2.getColumns().size(), 0);
        Assert.assertTrue(q2.getConditionals().isEmpty());
        Assert.assertTrue(q2.getJoins().isEmpty());
    }

    /**
     * Tests the addConditional method
     */
    @Test
    public void testAddConditional() {
        String tableName = "Users";
        SelectQuery q1 = new SelectQuery(tableName, colList1);
        Assert.assertTrue(q1.getConditionals().isEmpty());

        q1.addConditional(cond1);
        Assert.assertEquals(q1.getConditionals().size(), 1);
        Assert.assertEquals(q1.getConditionals().get(0), cond1);
    }

    /**
     * Tests the setJoins method
     */
    @Test
    public void testSetJoins() {
        String tableName = "Users";
        SelectQuery q1 = new SelectQuery(tableName, colList1);

        List<Join> joinList = new ArrayList<>();
        joinList.add(join1);

        Assert.assertTrue(q1.getJoins().isEmpty());

        q1.setJoins(joinList);

        Assert.assertEquals(q1.getJoins().size(), joinList.size());
        Assert.assertEquals(q1.getJoins(), joinList);
    }

    /**
     * Tests tha addJoin method
     */
    @Test
    public void testAddJoin() {
        String tableName = "Users";
        SelectQuery q1 = new SelectQuery(tableName, colList1);
        Assert.assertTrue(q1.getJoins().isEmpty());

        q1.addJoin(join1);
        Assert.assertEquals(q1.getJoins().size(), 1);
        Assert.assertEquals(q1.getJoins().get(0), join1);
    }

    /**
     * Tests render method
     */
    @Test
    public void testRender() {
        // Select all (*)
        String tableName = "Users";
        SelectQuery q1 = new SelectQuery(tableName);

        String output = q1.render();
        String correctOutput = "SELECT * FROM \"Users\";";

        Assert.assertEquals(output, correctOutput);

        // Specify Columns
        SelectQuery q2 = new SelectQuery(tableName, colList1);

        String output2 = q2.render();
        String correctOutput2 = "SELECT \"First Name\", \"Email\" FROM \"Users\";";

        Assert.assertEquals(output2, correctOutput2);

        // With conditional
        SelectQuery q3 = new SelectQuery(tableName);
        q3.addConditional(cond1);
        String output3 = q3.render();
        String correctOutput3 = "SELECT * FROM \"Users\" WHERE \"First Name\"='Jim';";

        Assert.assertEquals(output3, correctOutput3);

        // Multiple Conditionals
        SelectQuery q4 = new SelectQuery(tableName);
        q4.addConditional(cond1);
        q4.addConditional(cond2);
        String output4 = q4.render();
        String correctOutput4 = "SELECT * FROM \"Users\" WHERE \"First Name\"='Jim' AND \"Email\"!='test@email.com';";

        Assert.assertEquals(output4, correctOutput4);

    }

    @Test
    public void testRenderWithJoins() {
        // Select all (*)
        String tableName = "Logins";
        SelectQuery q1 = new SelectQuery(tableName);

        Join join = new InnerJoin("Users", col1, "Logins", col2);

        q1.addJoin(join);

        String output = q1.render();
        String correctOutput = "SELECT * FROM \"Logins\" INNER JOIN \"Users\" ON \"Users\".\"First Name\"=\"Logins\".\"Email\";";

        Assert.assertEquals(output, correctOutput);

    }
}
