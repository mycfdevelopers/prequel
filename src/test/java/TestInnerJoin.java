import com.mycf.prequel.Column;
import com.mycf.prequel.InnerJoin;
import com.mycf.prequel.Join;
import com.mycf.prequel.Type;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestInnerJoin {

    private static String table1 = "Users";
    private static String table2 = "Logins";
    private static Type type1 = new Type();
    private static Column col1 = new Column("Email", type1, false, false);
    private static Column col2 = new Column("User Email", type1, false, false);

    /**
     * Tests base constructor + gets
     */
    @Test
    public void testConstructorAndGets() {
        InnerJoin innerJoin = new InnerJoin(table2, col2, table1, col1);

        Assert.assertEquals(table2, innerJoin.getTableToJoinWith());
        Assert.assertEquals(col2, innerJoin.getReferenceColumn());
        Assert.assertEquals(table1, innerJoin.getLocalTable());
        Assert.assertEquals(col1, innerJoin.getLocalColumn());
    }

    @Test
    public void testSets() {
        InnerJoin innerJoin = new InnerJoin(table2, col2, table1, col1);

        // Table to Join With
        innerJoin.setTableToJoinWith(table1);
        Assert.assertEquals(table1, innerJoin.getTableToJoinWith());

        // Reference Column
        innerJoin.setReferenceColumn(col1);
        Assert.assertEquals(col1, innerJoin.getReferenceColumn());

        // Local Table
        innerJoin.setLocalTable(table2);
        Assert.assertEquals(table2, innerJoin.getLocalTable());

        // Local Column
        innerJoin.setLocalColumn(col2);
        Assert.assertEquals(col2, innerJoin.getLocalColumn());
    }

    @Test
    public void testRender() {
        InnerJoin innerJoin = new InnerJoin(table2, col2, table1, col1);

        String correctRender = "INNER JOIN \"Logins\" ON \"Logins\".\"User Email\"=\"Users\".\"Email\"";
        Assert.assertEquals(correctRender, innerJoin.render());
    }
}
