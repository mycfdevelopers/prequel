import com.mycf.prequel.Column;
import com.mycf.prequel.Conditional;
import com.mycf.prequel.Conditional.Operator;
import com.mycf.prequel.Type;
import com.mycf.prequel.exceptions.NoSuchOperatorException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestConditional {

    private static Type type1 = new Type();
    private static Column col1 = new Column("First Name", type1, false, false);
    private static Column col2 = new Column("Email", type1, true, false);

    /**
     * Tests the constructor and base get methods
     */
    @Test
    public void testConstructorAndGets() {
        String valueToCheck = "Jim";
        Conditional con1 = new Conditional(col1, Operator.EQUALS, valueToCheck);

        Assert.assertEquals(con1.getColumn(), col1);
        Assert.assertEquals(con1.getOperator(), Operator.EQUALS);
        Assert.assertEquals(con1.getValueToCheck(), valueToCheck);

        // No value to check
        Conditional con2 = new Conditional(col1, Operator.EQUALS);

        Assert.assertEquals(con2.getColumn(), col1);
        Assert.assertEquals(con2.getOperator(), Operator.EQUALS);
        Assert.assertNull(con2.getValueToCheck());
    }

    /**
     * Tests basic set methods
     */
    @Test
    public void testSets() {
        String valueToCheck = "Jim";
        Conditional con1 = new Conditional(col1, Operator.EQUALS, valueToCheck);

        // Column
        Assert.assertNotEquals(col1, col2);
        con1.setColumn(col2);
        Assert.assertEquals(con1.getColumn(), col2);

        // Operator
        Assert.assertNotEquals(Operator.EQUALS, Operator.NOT_EQUALS);
        con1.setOperator(Operator.NOT_EQUALS);
        Assert.assertEquals(con1.getOperator(), Operator.NOT_EQUALS);

        // Value to Check
        String valueToCheck2 = "Bob";
        Assert.assertNotEquals(valueToCheck, valueToCheck2);
        con1.setValueToCheck(valueToCheck2);
        Assert.assertEquals(con1.getValueToCheck(), valueToCheck2);
    }

    /**
     * Tests the getOperatorStr method
     */
    @Test
    public void testGetOperatorStr() throws NoSuchOperatorException {
        // Equals
        String equals = Conditional.getOperatorStr(Operator.EQUALS);
        Assert.assertEquals(equals, "=");

        // Not Equals
        String nEquals = Conditional.getOperatorStr(Operator.NOT_EQUALS);
        Assert.assertEquals(nEquals, "!=");
    }

    /**
     * Tests the getForQuery method
     */
    @Test
    public void testGetForQuery() {
        String valueToCheck = "Jim";
        Conditional con1 = new Conditional(col1, Operator.EQUALS, valueToCheck);

        String forQuery = con1.getForQuery();
        String correctForQuery = "\"First Name\"='Jim'";

        Assert.assertEquals(forQuery, correctForQuery);

        // No value to check
        Conditional con2 = new Conditional(col1, Operator.EQUALS);

        String forQuery2 = con2.getForQuery();
        String correctForQuery2 = "\"First Name\"=?";

        Assert.assertEquals(forQuery2, correctForQuery2);
    }
}
