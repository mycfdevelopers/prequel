import com.mycf.prequel.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class TestUtils {

    /**
     * Tests the 'join()' method.
     */
    @Test
    public void testJoin() {
        List<String> list1 = new ArrayList<String>();
        list1.add("Blue");
        list1.add("Red");
        list1.add("Green");

        String joined = Utils.join(list1, " and ");
        Assert.assertEquals(joined, "Blue and Red and Green");

        // Single element list
        List<String> list2 = new ArrayList<String>();
        list2.add("House");
        String joined2 = Utils.join(list2, ", ");
        Assert.assertEquals(joined2, "House");
    }
}
