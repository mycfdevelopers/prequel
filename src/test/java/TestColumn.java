import com.mycf.prequel.Column;
import com.mycf.prequel.Type;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestColumn {

    /**
     * Tests base constructor + gets
     */
    @Test
    public void testConstructorAndGets() {
        String name = "First Name";
        Type newType = new Type();
        Column col = new Column(name, newType, false, false);

        Assert.assertEquals(col.getName(), name);
        Assert.assertEquals(col.getType(), newType);
        Assert.assertFalse(col.isPrimaryKey());
        Assert.assertFalse(col.canBeNull());

        String name2 = "email";
        Column col2 = new Column(name2, newType, true, true);

        Assert.assertEquals(col2.getName(), name2);
        Assert.assertEquals(col2.getType(), newType);
        Assert.assertTrue(col2.isPrimaryKey());
        Assert.assertTrue(col2.canBeNull());
    }

    @Test
    public void testSets() {
        String name = "First Name";
        Type newType = new Type();
        Column col = new Column(name, newType, false, false);

        // Name
        String newName = "Last Name";
        col.setName(newName);
        Assert.assertEquals(col.getName(), newName);

        // Type
        Type newType2 = new Type();
        Assert.assertNotEquals(newType, newType2);
        col.setType(newType2);
        Assert.assertEquals(col.getType(), newType2);

        // Primary Key
        Assert.assertFalse(col.isPrimaryKey());
        col.setAsPrimaryKey();
        Assert.assertTrue(col.isPrimaryKey());
        col.setAsNotPrimaryKey();
        Assert.assertFalse(col.isPrimaryKey());

        // Can Be Null
        Assert.assertFalse(col.canBeNull());
        col.allowNullValues();
        Assert.assertTrue(col.canBeNull());
        col.disallowNullValues();
        Assert.assertFalse(col.canBeNull());
    }

    @Test
    public void testGetNameForQuery() {
        String name = "First Name";
        Type newType = new Type();
        Column col = new Column(name, newType, false, false);

        Assert.assertEquals(col.getNameForQuery(), "\"First Name\"");
    }
}
