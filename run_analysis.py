import subprocess
import os
import sys

# Run Tests (for Coverage report)
build_cmd = "mvn test"
print(build_cmd)
build_proc = subprocess.Popen(build_cmd.split(" "), shell=False)
build_proc.communicate()

key = "ed2f4eccc1f040efae802842bb7216dccee3062f"

prequel_location = "\\".join(os.path.realpath(__file__).split("\\")[0:-1])

if len(sys.argv) == 1:
    # No arguments passed
    from git import Repo
    repo = Repo(prequel_location)
    branch = repo.active_branch
else:
    branch = sys.argv[1]

options = {
    "sonar.projectKey": "mycfdevelopers_jsql",
    "sonar.organization": "mycfdevelopers",
    "sonar.projectName": "prequel",
    "sonar.host.url": "https://sonarcloud.io",
    "sonar.login": key,
    "sonar.branch.name": branch,
    "sonar.java.binaries": "target/classes",
    "sonar.sources": ["src/main/java"],
    "sonar.tests": "src/test",
    "sonar.coverage.jacoco.xmlReportPaths": "target/site/jacoco/jacoco.xml"
}

command = "mvn sonar:sonar "

for option in options:
    if isinstance(options[option], list):
        options[option] = ",".join(options[option])
    command += "-D{0}={1} ".format(option, options[option])

print(command)
proc = subprocess.Popen(command.split(" "), shell=False)
proc.communicate()
